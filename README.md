# ArLedController

A  Windows tray application for controlling color gradients of a Ws2812B Led strip via an Arduino

## Getting Started

* Clone code 
* set variables in Arduino/RainbowSerial.ino 
* write your Arduino/RainbowSerial.ino to your Arduino 
* set port in ArledController 
* compile your .exe

## Connect your WS2812B

* The DIN (data input) pin of the LED strip goes to Arduino PIN 3 with an optional 470Ω resistor in between.
* +5V of the LED strip goes to the VIM of extra power supply.
* GND of the LED strip goes to GND of the extra power supply and to the GND of the Arduino.
* The USB of the Arduino is connected to your computer.

## Author

* **[Andree Schubert](https://gitlab.com/A.Schubert)** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/A.Schubert/arledcontroller/-/blob/master/LICENSE) file for details
